INTRODUCTION
------------

The Wordbee Beebox connector is a plugin for the Translation Management Tool.
It connects a Drupal site with a Wordbee Beebox server for the purpose of
streamlining translation processes. The plugin sends translation requests
to a Beebox server and automatically receives back translations.

Website: https://www.wordbee.com

REQUIREMENTS
------------

This module requires the following modules:

 * TMGMT (https://www.drupal.org/project/tmgmt)
 
This module requires the following software:

 * Wordbee Beebox (https://www.wordbee.com/translation-connectors)
 
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure your Beebox credentials in Administration » Translation » Providers.
 
 * You can access our full documentation at:
   https://documents.wordbee.com/display/bb/Drupal
   
TROUBLESHOOTING
---------------

 * In case of defect, please log an issue on Drupal version control.
 
 * For any assistance, please contact our support helpdesk:
   https://wordbee.zendesk.com