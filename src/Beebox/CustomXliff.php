<?php

namespace Drupal\tmgmt_wordbee\Beebox;

use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_file\Plugin\tmgmt_file\Format\Xliff;

class CustomXliff extends Xliff {

  /**
   * Same as Xliff::addTransUnit() exept it leaves the target element empty
   *
   * @param string $key
   * @param array $element
   */
  protected function addTransUnit($key, $element, JobInterface $job) {
    $arrayKey = \Drupal::service('tmgmt.data')->ensureArrayKey($key);

    $this->startElement('trans-unit');
    $this->writeAttribute('id', $key);
    $this->writeAttribute('resname', $key);
    $this->startElement('source');
    $this->writeAttribute(
      'xml:lang',
      $this->job->getTranslator()->mapToRemoteLanguage(
        $this->job->source_language
      )
    );

    if ($job->getSetting('xliff_processing')) {
      $this->writeRaw($this->processForExport($element['#text'], $arrayKey));
    }
    else {
      $this->text($element['#text']);
    }

    $this->endElement();
    $this->startElement('target');
    $this->endElement();

    if (isset($element['#label'])) {
      $this->writeElement('note', $element['#label']);
    }

    $this->endElement();
  }

}
