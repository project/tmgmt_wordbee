<?php
/**
 * @file
 * Contains \Drupal\tmgmt_wordbee\WordbeeTranslatorUi.
 */

namespace Drupal\tmgmt_wordbee;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * Beebox translator UI.
 */
class BeeboxTranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(
    array $form,
    FormStateInterface $form_state,
    JobInterface $job
  ) {
    $form['unapprove_all'] = [
      '#type' => 'checkbox',
      '#title' => t('Skip Beebox memory'),
      '#description' => t(
        'When ticked, Beebox flags all translations to require human translation (thus disregarding any earlier human approvals).'
      ),
    ];
    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    $form = [];
    if ($job->isActive()) {
      $form['actions']['pull'] = [
        '#type' => 'submit',
        '#value' => t('Refresh job'),
        '#submit' => ['_tmgmt_wordbee_pull_submit'],
        '#weight' => -10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $module_infos = \Drupal::service('extension.list.module')->getExtensionInfo('tmgmt_wordbee');

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => 'Beebox URL',
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('url'),
      '#description' => 'Please enter the URL of your Beebox',
    ];
    $form['projectKey'] = [
      '#type' => 'textfield',
      '#title' => 'Project Key',
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('projectKey'),
      '#description' => 'Please enter your Beebox Account Key',
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => 'Username',
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('username'),
      '#description' => 'Please enter your Beebox Username',
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => 'Password',
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('password'),
      '#description' => 'Please enter your Beebox password',
    ];
    $form['leave_xliff_target_empty'] = [
      '#type' => 'checkbox',
      '#title' => 'Leave XLIFF files target element empty',
      '#default_value' => $translator->getSetting('leave_xliff_target_empty'),
      '#description' => 'If you don\'t know what to do with this option, just leave it checked ',
    ];
    $form['version'] = [
      '#type' => 'textfield',
      '#title' => 'Plugin version',
      '#default_value' => $module_infos['version'],
      '#description' => 'You can get the latest version of this connector on <a href="https://www.wordbee.com/beebox-downloads" target="_blank">www.wordbee.com/beebox-downloads</a>',
      '#disabled' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState
  ) {
    parent::validateConfigurationForm($form, $formState);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $formState->getFormObject()->getEntity();

    // check if we can reach the beebox
    $connected = $translator->getPlugin()->checkAvailable($translator);
    if ($connected->getSuccess()) {
      try {
        $translator->getPlugin()->configureCallback($translator);
      } catch (Exception $e) {
      }
    }
    else {
      $formState->setErrorByName(
        'settings][url',
        t('@message', ['@message' => $connected->getReason()])
      );
      $formState->setErrorByName(
        'settings][projectKey',
        t('@message', ['@message' => $connected->getReason()])
      );
      $formState->setErrorByName(
        'settings][username',
        t('@message', ['@message' => $connected->getReason()])
      );
      $formState->setErrorByName(
        'settings][password',
        t('@message', ['@message' => $connected->getReason()])
      );
    }
  }

}
