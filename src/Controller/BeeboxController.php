<?php

namespace Drupal\tmgmt_wordbee\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Wordbee Beebox translator CMS Callback controller.
 *
 * Check @link https://www.wordbee.com/beebox-downloads
 *
 * @version 1.0
 */
class BeeboxController extends ControllerBase {

  /**
   * Callback operation.
   * This method is called by Beebox when a content is ready to be downloaded.
   * Check if the job exists and get the latest status on Beebox.
   *
   * @param Request $request The HTTP request provided by Drupal.
   */
  public function callback(Request $request) {
    $eventType = $request->get('bbevent');
    $successFlag = $request->get('bbok') == "yes";
    $translatorId = $request->get('translator_id');
    $beeboxProject = $request->get('bbproject');
    $eventId = $request->get('bbid');

    if ($eventType != 'CreatedTranslatedFiles' || !$successFlag || empty($eventId)) {
      return;
    }

    // Load the translator
    $translator = Translator::load($translatorId);
    if ($translator == NULL || $translator->getSetting(
        'projectKey'
      ) != $beeboxProject) {
      throw new NotFoundHttpException;
    }

    // Load the job
    $jobId = $translator->getPlugin()->getJobFromEvent($translator, $eventId);
    if ($jobId == NULL) {
      throw new NotFoundHttpException;
    }

    $job = Job::load($jobId);
    if (!$job) {
      throw new NotFoundHttpException;
    }

    // Download the job using the usual workflow.
    $translator = $job->getTranslator()->getPlugin();
    $translator->updateCompletedJob($job);

    return new Response();
  }

}