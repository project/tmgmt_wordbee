<?php

namespace Drupal\tmgmt_wordbee\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\Translator\TranslatableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt_file\Plugin\tmgmt_file\Format\Xliff;
use Drupal\tmgmt_wordbee\Beebox\BeeboxAPI;
use Drupal\tmgmt_wordbee\Beebox\CustomXliff;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wordbee Beebox translator plugin.
 *
 * Check @link https://www.wordbee.com/beebox-downloads
 *
 * @TranslatorPlugin(
 *   id = "wordbee",
 *   label = @Translation("Wordbee Beebox"),
 *   description = @Translation("WordBee Beebox Translation service."),
 *   ui = "Drupal\tmgmt_wordbee\BeeboxTranslatorUi",
 *   logo = "icons/wordbee.svg"
 * )
 */
class BeeboxTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * API_Calls instance
   *
   * @var BeeboxAPI
   */
  private $apiCalls;

  /**
   * Constructor
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($this->prepareApiCalls($translator)) {
      try {
        $this->apiCalls->connect();
        $result = AvailableResult::yes();
      } catch (TMGMTException $e) {
        $trace = $e->getTrace();

        if (($element = array_shift($trace))
          && isset($element['args'][1])
          && ($json = json_decode($element['args'][1]['response']))
          && array_key_exists('message', $json)
        ) {
          $message = $json->message;
        }
        else {
          $message = $e->getMessage();
        }
        $result = AvailableResult::no(
          t(
            '@translator is not configured correctly. Please <a href=:configured>check your credentials</a>.<br>Details : @message',
            [
              '@translator' => $translator->label(),
              ':configured' => $translator->toUrl()->toString(),
              '@message' => $message,
            ]
          )
        );
      }

      $this->apiCalls->disconnect();
    }
    else {
      $result = AvailableResult::no(
        t(
          '@translator is not configured yet. Please <a href=:configured>configure</a> the connector first.',
          [
            '@translator' => $translator->label(),
            ':configured' => $translator->toUrl()->toString(),
          ]
        )
      );
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function checkTranslatable(
    TranslatorInterface $translator,
    JobInterface $job
  ) {
    $this->prepareApiCalls($translator);
    try {
      $languages = $this->apiCalls->getProjectLanguages();
      $this->apiCalls->disconnect();

      $source = $translator->mapToRemoteLanguage(
        $job->getSourceLanguage()->getId()
      );
      if (!isset($languages[$source])) {
        return TranslatableResult::no(
          t(
            'The source language @sourcelocale is not the Beebox source language',
            ['@sourcelocale' => $job->getSourceLanguage()->getName()]
          )
        );
      }
      elseif (!array_key_exists(
        $translator->mapToRemoteLanguage($job->getTargetLanguage()->getId()),
        $languages[$source]
      )) {
        return TranslatableResult::no(
          t(
            'The target language @targetlocale is not configured in the Beebox project',
            ['@targetlocale' => $job->getTargetLanguage()->getName()]
          )
        );
      }
      return parent::checkTranslatable($translator, $job);
    } catch (TMGMTException $e) {
      return TranslatableResult::no(
        t('An error occured when we tried to check the project languages')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    if ($job->getTranslator()->getSetting('leave_xliff_target_empty')) {
      $fileformat = new CustomXliff(
        $this->configuration,
        $this->pluginId,
        $this->pluginDefinition
      );
    }
    else {
      $fileformat = new Xliff(
        $this->configuration,
        $this->pluginId,
        $this->pluginDefinition
      );
    }

    $filename = $job->id() . '-' . uniqid() . '-drupal_connector.xliff';
    // For now, we expect only a single mapping for a job, use that for the
    // filename info.
    $data = [
      'tjid' => $job->id(),
      'remote_identifier_1' => $filename,
    ];
    $remote_mapping = RemoteMapping::create($data);
    $remote_mapping->save();

    $xliffFile = $fileformat->export($job);

    $translator = $job->getTranslator();
    $this->prepareApiCalls($translator);

    $instructions = [
      'locales' => [
        $job->getTranslator()->mapToRemoteLanguage(
          $job->getTargetLanguage()->getId()
        ),
      ],
    ];

    if ($job->getSetting('unapprove_all') === 1) {
      $instructions['unapproveAll'] = TRUE;
    }

    try {
      $this->apiCalls->sendFile(
        $xliffFile,
        $filename,
        $job->getTranslator()->mapToRemoteLanguage(
          $job->getSourceLanguage()->getId()
        )
      );
      $this->apiCalls->sendFile(
        json_encode($instructions),
        $filename . '.beebox',
        $job->getTranslator()->mapToRemoteLanguage(
          $job->getSourceLanguage()->getId()
        )
      );
      $job->submitted('Job has been submitted to Beebox.');

    } catch (TMGMTException $e) {
      // Backwards compatibility of error logging. See
      // https://www.drupal.org/node/2932520. This can be removed when we no
      // longer support Drupal < 10.1.
      if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
        $logger = \Drupal::logger('tmgmt_wordbee');
        Error::logException($logger, $e);
      }
      else {
        // @phpstan-ignore-next-line
        watchdog_exception('tmgmt_wordbee', $e);
      }
      $job->rejected(
        'Job has been rejected with following error: @error',
        ['@error' => $e->getMessage()],
        'error'
      );
    }
    $this->apiCalls->disconnect();
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job) {
    $remotes = $job->getRemoteMappings();
    if (!empty($remotes)) {
      $remote = reset($remotes);
      $filename = $remote->getRemoteIdentifier1();
    }
    // Fallback to old filename pattern.
    if (!$filename) {
      $filename = $job->id() . '-drupal_connector.xliff';
    }
    $this->prepareApiCalls($job->getTranslator());
    try {
      $workprogress = $this->apiCalls->getWorkprogress([$filename]);
      if (count($workprogress) > 0) {
        $this->apiCalls->deleteFile(
          $filename,
          $job->getTranslator()->mapToRemoteLanguage(
            $job->getSourceLanguage()->getId()
          )
        );
        $this->apiCalls->deleteFile(
          $filename . '.beebox',
          $job->getTranslator()->mapToRemoteLanguage(
            $job->getSourceLanguage()->getId()
          )
        );
        $job->aborted('Job removed');
        $this->apiCalls->disconnect();

        return TRUE;
      }
    } catch (TMGMTException $e) {
      // Backwards compatibility of error logging. See
      // https://www.drupal.org/node/2932520. This can be removed when we no
      // longer support Drupal < 10.1.
      if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
        $logger = \Drupal::logger('tmgmt_wordbee');
        Error::logException($logger, $e);
      }
      else {
        // @phpstan-ignore-next-line
        watchdog_exception('tmgmt_wordbee', $e);
      }
      $job->rejected(
        'Job has not been cancelled with following error: @error',
        ['@error' => $e->getMessage()],
        'error'
      );
      $this->apiCalls->disconnect();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   * Used to show the remote language list in the configuration page.
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    if ($this->prepareApiCalls($translator)) {
      try {
        $remoteLanguages = $this->apiCalls->getProjectLanguages();
        $sourceLanguage = array_keys($remoteLanguages)[0];
        $allLanguages = [$sourceLanguage => $sourceLanguage];

        foreach (array_keys(
                   $remoteLanguages[$sourceLanguage]
                 ) as $id => $lang) {
          $allLanguages[$lang] = $lang;
        }

        return $allLanguages;
      } catch (TMGMTException $e) {
        // Backwards compatibility of error logging. See
        // https://www.drupal.org/node/2932520. This can be removed when we no
        // longer support Drupal < 10.1.
        if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
          $logger = \Drupal::logger('tmgmt_wordbee');
          Error::logException($logger, $e);
        }
        else {
          // @phpstan-ignore-next-line
          watchdog_exception('tmgmt_wordbee', $e);
        }
      }
      $this->apiCalls->disconnect();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTargetLanguages(
    TranslatorInterface $translator,
    $source_language
  ) {
    $this->prepareApiCalls($translator);
    $remoteLanguages = $this->apiCalls->getProjectLanguages();

    if (array_key_exists($source_language, $remoteLanguages)) {
      return $remoteLanguages[$source_language];
    }

    $this->apiCalls->disconnect();

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return FALSE;
  }

  /**
   * Check if a job is completed in Beebox and download the file.
   *
   * @param JobInterface $job The job to download files
   *
   * @return int The number of files downloaded.
   */
  public function updateCompletedJob(JobInterface $job) {
    $this->prepareApiCalls($job->getTranslator());
    $remotes = $job->getRemoteMappings();
    if (!empty($remotes)) {
      $remote = reset($remotes);
      $filename = $remote->getRemoteIdentifier1();
    }
    // Fallback to old filename pattern.
    if (!$filename) {
      $filename = $job->id() . '-drupal_connector.xliff';
    }
    $content = $this->apiCalls->getWorkprogress(
      [$filename]
    );
    $workprogress = json_decode($content);
    $fileformat = new Xliff(
      $this->configuration,
      $this->pluginId,
      $this->pluginDefinition
    );

    /** @var $acceptedTranslations int The number of translations accepted */
    $acceptedTranslations = 0;
    foreach ($workprogress->files as $work) {
      if ($work->uptodate) {
        $file = $this->apiCalls->getFile($work->file, $work->locale);
        if ($fileformat->validateImport($file, FALSE)) {
          $acceptedTranslations++;
          $job->addTranslatedData($fileformat->import($file, FALSE));
          if ($job->getTranslator()->isAutoAccept()) {
            $job->finished(
              'Translation downloaded from Beebox and auto accepted'
            );
          }
          else {
            $job->addMessage('Translation downloaded from Beebox');
          }
        }
      }
    }
    $this->apiCalls->disconnect();

    return $acceptedTranslations;
  }

  /**
   * Get the Job ID of a Drupal job from a received Beebox callback event.
   *
   * @param TranslatorInterface $translator The translator
   * @param int $eventId The Beebox event GUID
   */
  public function getJobFromEvent(TranslatorInterface $translator, $eventId) {
    if ($this->prepareApiCalls($translator)) {
      $event = $this->apiCalls->getEvent($eventId);
      $path = $event->details->files[0]->spath;
      $path = explode('\\', $path);
      $path = explode('-', end($path), 2);
      return reset($path);
    }
    $this->apiCalls->disconnect();
  }

  /**
   * Configure Beebox to send a callback to the current Drupal installation.
   *
   * @param TranslatorInterface $translator The translator
   */
  public function configureCallback(TranslatorInterface $translator) {
    if ($this->prepareApiCalls($translator)) {
      $drupalUri = Url::fromRoute(
        'tmgmt_wordbee.callback',
        ['translator_id' => $translator->id()]
      )->setOptions(
        ['absolute' => TRUE]
      )->toString();

      $this->apiCalls->updateCallback($drupalUri);
    }
    $this->apiCalls->disconnect();
  }

  /**
   * Instanciate BeeboxAPI if needed, this method should be called before using
   * $this->apiCalls
   */
  private function prepareApiCalls(TranslatorInterface $translator) {
    // 1. Check if we got all settings.
    if (!$translator->getSetting('url')
      || !$translator->getSetting('projectKey')
      || !$translator->getSetting('username')
      || !$translator->getSetting('password')) {
      return FALSE;
    }

    // 2. Connect
    $module_infos = \Drupal::service('extension.list.module')->getExtensionInfo('tmgmt_wordbee');
    if (!isset($this->apiCalls)) {
      $this->apiCalls = new BeeboxAPI(
        'WB-Drupal',
        $module_infos['version'],
        $translator->getSetting('url'),
        $translator->getSetting('projectKey'),
        $translator->getSetting('username'),
        $translator->getSetting('password')
      );
    }

    return TRUE;
  }

}
